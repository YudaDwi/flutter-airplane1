part of 'services.dart';

class UserServices {
  //ini akan menunjukan colection di cloud firestore
  CollectionReference _userReference =
      FirebaseFirestore.instance.collection('users');

  //ini seperti key dan value di cloud firestore
  //untuk mengatur setUser di cloud firestore
  Future<void> setUser(UserModel user) async {
    try {
      _userReference.doc(user.id).set({
        'email': user.email,
        'name': user.name,
        'hobby': user.hobby,
        'balance': user.balance
      });
    } catch (e) {
      throw e;
    }
  }

  //sedangkan ini untuk mengambil user sesuai id
  Future<UserModel> getUserById(String id) async {
    try {
      //userReference ini data dari cloud firestore
      DocumentSnapshot snapshot = await _userReference.doc(id).get();
      return UserModel(
          id: id,
          email: snapshot['email'],
          name: snapshot['name'],
          hobby: snapshot['hobby'],
          balance: snapshot['balance']);
    } catch (e) {
      throw e;
    }
  }
}
