part of 'services.dart';

class AuthServices {
  //mengembalikan instance menggunakan firebaseapp defautl
  static FirebaseAuth _auth = FirebaseAuth.instance;

  static Future<UserModel> signIn(
      {required String email, required String password}) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(
          email: email, password: password);

      //kalau signup menambahkan ini cuma mengambil
      UserModel user =
          await UserServices().getUserById(userCredential.user!.uid);
      return user;
    } catch (e) {
      throw e;
    }
  }

//buat fungsi signup
  static Future<UserModel> signUp(
      {required String email,
      required String password,
      required String name,
      String hobby = ''}) async {
    try {
      UserCredential userCredential = await _auth
          .createUserWithEmailAndPassword(email: email, password: password);

      //menambahkan ke cloud firestore
      UserModel user = UserModel(
          id: userCredential.user!.uid,
          email: email,
          name: name,
          hobby: hobby,
          balance: 280000000);

      await UserServices().setUser(user);
      return user;
    } catch (e) {
      throw e;
    }
  }

  static Future<void> signOut() async {
    try {
      await _auth.signOut();
    } catch (e) {
      throw e;
    }
  }
}

//try catch yaitu mengurung eksekusi yg menampilkan eror
//dan dapat membuat program tetap berjalan tanpa dihentikan secara langsung