import 'package:bloc/bloc.dart';
// import 'package:meta/meta.dart';

//40 mungkin karena cuma perpindahan page jadi hanya pakai int
//dan inisialisasi nya dimulai dari 0
class PageCubit extends Cubit<int> {
  PageCubit() : super(0);

  void setPage(int newPage) {
    emit(newPage);
  }
}
