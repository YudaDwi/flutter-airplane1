import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_airplane_1/models/models.dart';
import 'package:flutter_airplane_1/services/services.dart';
import 'package:meta/meta.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(AuthInitial());

  void signIn({required String email, required String password}) async {
    try {
      emit(AuthLoading());
      UserModel user =
          await AuthServices.signIn(email: email, password: password);
      emit(AuthSucces(user));
    } catch (e) {
      AuthFailed(e.toString());
    }
  }

  void signUp(
      {required String email,
      required String password,
      required String name,
      String hobby = ''}) async {
    try {
      emit(AuthLoading());

      //buat objek baru

      UserModel user = await AuthServices.signUp(
          email: email, password: password, name: name, hobby: hobby);
      //kalau berhasil maka akan di emit authsucces
      emit(AuthSucces(user));
    } catch (e) {
      //kalau gagal akan di emit authfailed
      emit(AuthFailed(e.toString()));
    }
  }

  void signOut() async {
    try {
      emit(AuthLoading());
      await AuthServices.signOut();
      //jadi nantinya ini akan jadi authinitial lagi
      emit(AuthInitial());
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }

  void getCurrentUser(String id) async {
    try {
      UserModel user = await UserServices().getUserById(id);
      //kalau berhasil akan emit
      emit(AuthSucces(user));
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }
}
