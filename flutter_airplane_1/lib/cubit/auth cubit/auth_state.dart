part of 'auth_cubit.dart';

@immutable
abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object?> get props => [];
}

class AuthInitial extends AuthState {}

//ketika kita melakukan loading state nya kita ganti menjadi authstate
class AuthLoading extends AuthState {}

class AuthSucces extends AuthState {
  final UserModel user;

  AuthSucces(this.user);
  @override
  List<Object?> get props => [user];
}

class AuthFailed extends AuthState {
  final String error;
  AuthFailed(this.error);

  @override
  List<Object?> get props => [error];
}

//proses ini sama dgn di flutter food