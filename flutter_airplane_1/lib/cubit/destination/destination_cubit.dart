import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_airplane_1/models/models.dart';
import 'package:flutter_airplane_1/services/services.dart';
import 'package:meta/meta.dart';

part 'destination_state.dart';

class DestinationCubit extends Cubit<DestinationState> {
  DestinationCubit() : super(DestinationInitial());

  void fetchDestinations() async {
    //caranya hampir mirip dgn auth cubit
    try {
      emit(DestinationLoading());
      List<DestinationModel> destinations =
          await DestinationServices().fetchDestinations();
      emit(DestinationSucces(destinations));
    } catch (e) {
      DestinationsFailed(e.toString());
    }
  }
}
