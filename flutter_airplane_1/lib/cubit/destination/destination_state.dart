part of 'destination_cubit.dart';

@immutable
abstract class DestinationState extends Equatable {
  @override
  List<Object?> get props => [];
}

class DestinationInitial extends DestinationState {}

//pembuatannya mirip dgn auth state dibutuhkan 3 state

class DestinationLoading extends DestinationState {}

class DestinationSucces extends DestinationState {
  final List<DestinationModel> destinations;

  DestinationSucces(this.destinations);
  @override
  List<Object?> get props => [destinations];
}

class DestinationsFailed extends DestinationState {
  final String eror;
  DestinationsFailed(this.eror);

  @override
  List<Object?> get props => [eror];
}
