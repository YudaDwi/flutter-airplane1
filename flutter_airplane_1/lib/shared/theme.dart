part of 'shared.dart';

double defaultMargin = 24;
double defaultRadius = 17;

//3 0x untuk memberitahu bahwa kita menggunakan hexcode,ff memberitau bahwa warna opacity itu 100%
Color kPrimaryColor = Color(0xff5C40CC);
Color kBlackColor = Color(0xff1F1449);
Color kWhiteColor = Color(0xffFFFFFF);
Color kGreyColor = Color(0xff9698A9);
Color kGreenColor = Color(0xff0EC3AE);
Color kRedColor = Color(0xffEB70A5);
Color kTransparentColor = Colors.transparent;
Color kUnAvailableColor = Color(0xffEBECF1);
Color kAvailableColor = Color(0xffE0D9FF);

Color kBackgroundColor = Color(0xffFAFAFA);
//3 ini untuk stroke pada textfield
Color kinactiveColor = Color(0xffDBD7EC);

TextStyle blackTextStyle = GoogleFonts.poppins(color: kBlackColor);
TextStyle whiteTextStyle = GoogleFonts.poppins(color: kWhiteColor);
TextStyle greyTextStyle = GoogleFonts.poppins(color: kGreyColor);
TextStyle greenTextStyle = GoogleFonts.poppins(color: kGreenColor);
TextStyle redTextStyle = GoogleFonts.poppins(color: kRedColor);
TextStyle purpleTextStyle = GoogleFonts.poppins(color: kPrimaryColor);

FontWeight light = FontWeight.w300;
FontWeight reguler = FontWeight.w400;
FontWeight medium = FontWeight.w500;
FontWeight semiBold = FontWeight.w600;
FontWeight bold = FontWeight.w700;
FontWeight extraBolc = FontWeight.w800;
FontWeight blac = FontWeight.w900;
