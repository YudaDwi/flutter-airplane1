part of 'pages.dart';

class SuccesCheckoutPage extends StatelessWidget {
  const SuccesCheckoutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget imageSucces() {
      return Container(
        margin: EdgeInsets.only(bottom: 80),
        height: 150,
        width: 300,
        decoration: BoxDecoration(
            image:
                DecorationImage(image: AssetImage('assets/image_success.png'))),
      );
    }

    Widget myBookingButton() {
      return CustomButton(
        title: 'My Bookings',
        //setelah kita melakukan navigasi ke route main , nah untuk route sebelumnya akan di remove
        //sehingga kita tidak bisa kembali ke route sebelumnya
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(context, '/main', (route) => false);
        },
        margin: EdgeInsets.symmetric(vertical: 50),
        width: 220,
      );
    }

    return Scaffold(
      backgroundColor: kBackgroundColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            imageSucces(),
            Text(
              'Well Booked 😍',
              style:
                  blackTextStyle.copyWith(fontSize: 32, fontWeight: semiBold),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Are you ready to explore the new\nworld of experiences?',
              style: greyTextStyle.copyWith(fontSize: 16, fontWeight: light),
              textAlign: TextAlign.center,
            ),
            myBookingButton()
          ],
        ),
      ),
    );
  }
}
