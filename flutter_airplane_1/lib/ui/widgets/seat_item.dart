part of 'widgets.dart';

class SeatItem extends StatelessWidget {
  //catatan
  //0=available 1=selected 2=unavailable
  final int status;
  const SeatItem({Key? key, required this.status}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    backgroundColor() {
      switch (status) {
        case 0:
          return kAvailableColor;
        case 1:
          return kPrimaryColor;
        case 2:
          return kUnAvailableColor;
        default:
          return kUnAvailableColor;
      }
    }

    borderColor() {
      switch (status) {
        case 0:
          return kPrimaryColor;
        case 1:
          return kPrimaryColor;
        case 2:
          return kUnAvailableColor;
        default:
          return kUnAvailableColor;
      }
    }
    //32 ini kalo pak erico pasti pake if else
    //sebenarnya mirip , malah tak pikir pakai cara pak erico
    //ternyata ini lebih rapi

    child() {
      switch (status) {
        case 0:
          return SizedBox();
        case 1:
          return Center(
            child: Text(
              'YOU',
              style: whiteTextStyle.copyWith(fontWeight: semiBold),
            ),
          );
        case 2:
          return SizedBox();
        default:
          return SizedBox();
      }
    }

    return Container(
      height: 48,
      width: 48,
      decoration: BoxDecoration(
          color: backgroundColor(),
          borderRadius: BorderRadius.circular(15),
          border: Border.all(width: 2, color: borderColor())),
      child: Center(child: child()),
    );
  }
}
